<%@ include file="/WEB-INF/jspf/page.jspf" %>
<c:if test='${empty webUser || !webUser.loggedIn}'>
  <c:redirect url='../login.jsp'/>
</c:if>
<html lang="en">
<%@ include file="/WEB-INF/jspf/head.jspf" %>
<title><c:out value='${ctx.name}'/> - My Receipts</title>
</head>
<body class="nav-md">
    <div class="container body">
      <div class="main_container">
<%@ include file="/WEB-INF/jspf/menu.jspf" %>
<!-- header -->
<%@ include file="/WEB-INF/jspf/header.jspf" %>
        <div class="right_col" role="main">
		    <div class="">
		        <div class="page-title">
		            <div class="row">
		                <div class="col-md-12 col-sm-12 col-xs-12">
		                    <div class="x_panel">
		                        <div class="x_title">
		                            <div class="title_left">
		                                <h3><i class="fa fa-truck"></i> Prijemky <small> na spracovanie</small></h3>
		                            </div>
		                            <div class="clearfix"></div>
		                        </div>
		                    </div>
		                </div>
		            </div>
		        </div>
		    </div>
		    <div class="clearfix"></div>


            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_content">
      			<form  name="products" id="products">
        			
                    <table id="Customerreceipts" class="table table-striped table-bordered">
                      <thead>
				          <tr> 
					          <th>C.</th>
					          <th>Datum</th>
					          <th>Partner</th>
					          <th>Stav</th>
				          </tr>
                      </thead>
	                      <tbody>
					        <c:forEach items='${infowms.inbound}' var='receipt' varStatus='status'>
					        	<jsp:useBean id="status" type="javax.servlet.jsp.jstl.core.LoopTagStatus" />
					        	<c:choose>
					        		<c:when test="<%= status.getCount() %2 == 0 %>">
						        		<c:set var="rowClass" value="evenRow"/>
					        		</c:when>
					        		<c:otherwise>
						        		<c:set var="rowClass" value="oddRow"/>
					        		</c:otherwise>
					        	</c:choose> 
					        <tr> 
					          <td class="<c:out value='${rowClass}' />"><a href="receiptDetails.jsp?M_InOut_ID=<c:out value='${receipt.m_InOut_ID}'/>"><c:out value='${receipt.documentNo}'/></a></td>
					          <td class="<c:out value='${rowClass}' />"><fmt:formatDate value='${receipt.movementDate}'/></td>
			          	      <td class="<c:out value='${rowClass}' />"><c:out value='${receipt.BPName}'/></td>
					          <td class="<c:out value='${rowClass}' />"><c:out value='${receipt.docStatusName}'/></td>
					          
					        </tr>
					        </c:forEach> 
	                       </tbody>
	                   </table>
                   	</form>
                  </div>
                </div>
              </div>
            </div>
          </div>
			<!-- /page content -->
			<%@ include file="/WEB-INF/jspf/footer.jspf"%>
		</div>
	</div>
	<%@ include file="/WEB-INF/jspf/footnx.jspf"%>
		
</body>
</html>