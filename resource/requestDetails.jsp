<%@ include file="/WEB-INF/jspf/page.jspf" %>
<%@ include file="/WEB-INF/jspf/page.jspf" %>
<c:if test='${empty webUser || !webUser.loggedIn}'>
  <c:redirect url='loginServlet?ForwardTo=login.jsp'/>
</c:if>
<html>
<html lang="en">
<%@ include file="/WEB-INF/jspf/head.jspf" %>
<title><c:out value='${ctx.name}'/> - Help</title>
<script type="text/javascript">
function validateRequest()
{
    if(document.Request.Summary.value == '')
    {
        alert("Follow-Up field is mandatory.");
        return false;
    }
    return true;
}
function responseChanged()
{
	var doingRequest = document.Request.Close.checked;
	doingRequest |= document.Request.Escalate.checked;
	doingRequest |= document.Request.Confidential.checked;
	doingRequest |= (document.Request.Summary.value != '');
	setAttachmentDisabled(doingRequest);
}
function attachmentChanged()
{
	setResponseDisabled( document.fileLoad.file.value != '' );
}
function setResponseDisabled(disabled)
{
	document.Request.Close.disabled = disabled;
	document.Request.Escalate.disabled = disabled;
	document.Request.Confidential.disabled = disabled;
	document.Request.Summary.disabled = disabled;
	document.Request.Reset.disabled = disabled;
	document.Request.Submit.disabled = disabled;
	document.getElementById("responseDisabled").style.display=(disabled?"block":"none");
}
function setAttachmentDisabled(disabled)
{
	document.fileLoad.file.disabled = disabled;
	document.fileLoad.Submit.disabled = disabled;
	document.getElementById("attachDisabled").style.display=(disabled?"block":"none");
}
</script>
</head>
</head>
<body class="nav-md">
    <div class="container body">
        <div class="main_container">
            <%@ include file="/WEB-INF/jspf/menu.jspf" %>
                <!-- header -->
                <%@ include file="/WEB-INF/jspf/header.jspf" %>

                    <!-- page content -->
                    <div class="right_col" role="main">
                        <div class="row">

                            <div class="col-md-12 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_title">
<!--                                         <h2>My requests</h2> -->
<!--                                         <a href="request.jsp"> -->
<!--                                             <button type="submit" class="btn btn-success" style="float: right;"> New request</button> -->
<!--                                         </a> -->
                                        <div class="clearfix"></div>

                                    </div>
                                    <div class="x_content">
<!-- Set Request ID and get Request		-->
<c:set target='${info}' property='id' value='${param.R_Request_ID}' />
<c:set var='request' value='${info.request}' />
<c:if test='${empty request}'>
  <c:set target='${info}' property='message' value='Request not found' />
  <c:redirect url='requests.jsp'/>
</c:if>

<div id="main">
	<div id="content"> 
	  <h1>Poziadavka <c:out value='${request.documentNo}'/></h1>
	  <c:if test='${not empty info.info}'>
	    <p><c:out value='${info.message}' /></p>
	  </c:if>
      <fieldset>
<!--           <legend>Poziadavka</legend> -->
          <table class="table table-striped table-bordered">
              <tr><td colspan="3" class="lineItem">
                  <span class="emphasized">Popis: </span>
                  <pre><c:out value='${request.summary}' escapeXml="false" /></pre>
                  <br/><br/>&nbsp;
              </td></tr>
                <tr>
                    <td class="lineItem">
                        <label>Typ: </label><c:out value='${request.requestTypeName}'/><br/>
                        <label>Stav: </label><c:out value='${request.statusName}'/><br/>
                    </td>
                    <td class="lineItem">
                        <label>Datum vytvorenia: </label><fmt:formatDate value='${request.created}'/><br/>
                        <label>Vytvorena uzivatelom: </label><c:out value='${request.createdByName}'/><br/>
                    </td>
                    <td class="lineItem">
                        <fieldset>
                            <legend>Prilohy</legend>
                            <c:if test='${not empty request.attachment}'>
                            	<c:if test='${not empty request.attachment.textMsg}'>
	                                <label><c:out value='${request.attachment.textMsg}'/></label>
	                            </c:if>
                                <c:forEach items='${request.attachment.entries}' var='entry'>
                                    <a href="requestServlet?R_Request_ID=<c:out value='${request.r_Request_ID}'/>&AttachmentIndex=<c:out value='${entry.index}'/>" target="_blank">
                                            <c:out value='${entry.name}'/>
                                    </a><br/>
                                </c:forEach>
                            </c:if>
                        </fieldset>
                    </td>
                </tr>
            </table>
      </fieldset>

      <br/><br/>

      <c:if test='${request.webCanUpdate}'>
      <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
 
 
 <a class="btn btn-primary" role="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
  Odpoved / doplnenie k poziadavke 
</a>

<div class="collapse" id="collapseExample">
  <div class="well">
  
       
        </a>
      </h4>
    </div>
    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
      <div class="panel-body">
      <form method="post" name="Request" action="requestServlet" enctype="application/x-www-form-urlencoded"
			onSubmit="return validateRequest()">

          <input name="Source" type="hidden" value=""/>
          <input name="Info" type="hidden" value=""/>
          <script language="Javascript">
            document.Request.Source.value=document.referrer;
            document.Request.Info.value=document.lastModified;
          </script>
          <input name="ForwardTo" type="hidden" value="<c:out value='${param.ForwardTo}'/>"/>
          <input name="SalesRep_ID" type="hidden" value="<c:out value='${webUser.salesRep_ID}'/>"/>
          <input name="R_Request_ID" type="hidden" id="R_Request_ID" value="<c:out value='${request.r_Request_ID}'/>" />

          <fieldset>

              <label>Od</label>
              <c:out value='${webUser.name}'/> / <c:out value='${webUser.email}'/>
              <br/>

              <label>Akcia</label>
              <input name="Close" type="checkbox" id="Close" value="Close" onclick="responseChanged()">Uzavriet poziadavku
              <br/>

<!--               <label>&nbsp;</label> -->
<!--               <input name="Escalate" type="checkbox" id="Escalate" value="Escalate" onclick="responseChanged()">Eskalovat poziadavku -->
<!--               <br/> -->


              <fieldset>
<!--                   <legend>Follow-Up</legend> -->
                  <textarea name="Summary" cols="80" rows="8" id="ID_Summary" class="wideText" onkeyup="responseChanged()"></textarea>
                  <div class="entryNote">
                      Odpoved: 1500 znakov max; pre dlhsie texty pouzite prilohy
                  </div>
              </fieldset>

			<div id="responseDisabled" class="disabledMsg">Odpoved nepovolena</div>
              <div class="buttons">
                  <input name="Reset" type="reset" value="Reset" onclick="document.Request.reset();responseChanged();"/>
                  <input name="Submit" type="submit" value="Submit">
              </div>
          </fieldset>
      </form>

      <br/><br/>

	  <form action="requestServlet" method="post" enctype="multipart/form-data" name="fileLoad" id="fileLoad" onreset="attachmentChanged()">
	  	<fieldset>
	  		<legend>Attach File</legend>
			<input name="R_Request_ID" type="hidden" id="R_Request_ID" value="<c:out value='${request.r_Request_ID}'/>">
			<div id="attachDisabled" class="disabledMsg">Prilohu nieje mozne pridat , na poziadavke sa prave pracuje</div>
			<label for="file">Priloha: </label>
			<input name="file" type="file" id="file" size="40" onchange="attachmentChanged()" onkeyup="attachmentChanged()">
			<input type="submit" name="Submit" value="Upload">
	  	</fieldset>
      </form>

	</div>  
  </div>
</div>
      <br/><br/>


	  </c:if>
	  <p>&nbsp;</p>
	  <h3>Historia komunikacie</h3>
      <table class="table table-striped table-bordered" >
        <tr> 
          <th>Vytvorena datum</th>
          <th>Uzivatel</th>
          <th>Stav</th>
        </tr>
        <c:forEach items='${request.updatesCustomer}' var='update' varStatus='status'>
        <tr> 
          <td class="<c:out value='${rowClass}' />"><fmt:formatDate value='${update.created}'/></td>
          <td class="<c:out value='${rowClass}' />"><c:out value='${update.createdByName}'/></td>
          <td class="<c:out value='${rowClass}' />"><c:out value='${update.result}'/>&nbsp;</td>
        </tr>
        </c:forEach> 
      </table>
	  <br>
       <table class="table table-striped table-bordered"> 
        <tr> 
          <th>Upravena</th>
          <th>Uzivatel</th>
          <th>Stara hodnota</th>
        </tr>
        <c:forEach items='${request.actions}' var='action' varStatus='status2'>
     
        <tr> 
          <td class="<c:out value='${rowClass}' />"><fmt:formatDate value='${action.created}'/></td>
          <td class="<c:out value='${rowClass}' />"><c:out value='${action.createdByName}'/></td>
          <td class="<c:out value='${rowClass}' />"><c:out value='${action.changesHTML}' escapeXml='false'/>&nbsp;</td>
        </tr>
        </c:forEach> 
      </table>
                        <!-- /page content -->

                    </div>
        </div>
			<!-- /page content -->
			<%@ include file="/WEB-INF/jspf/footer.jspf"%>
		</div>
	</div>
	<%@ include file="/WEB-INF/jspf/footnx.jspf"%>
</body>
</html>
