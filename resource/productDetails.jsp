<%@ include file="/WEB-INF/jspf/page.jspf"%>
<c:if test='${empty webUser || !webUser.loggedIn}'>
	<c:redirect url='loginServlet?ForwardTo=login.jsp' />
</c:if>
<script type="text/javascript"
	src="/umsb2b/highslide/highslide-with-gallery.js"></script>
<link rel="stylesheet" type="text/css"
	href="/umsb2b/highslide/highslide.css" />


<script type="text/javascript">
	hs.graphicsDir = '/umsb2b/highslide/graphics/';
	hs.align = 'center';
	hs.transitions = [ 'expand', 'crossfade' ];
	hs.fadeInOut = true;
	hs.outlineType = 'glossy-dark';
	hs.wrapperClassName = 'dark';
	hs.captionEval = 'this.a.title';
	hs.numberPosition = 'caption';
	hs.useBox = true;
	hs.width = 800;
	hs.height = 800;
	//hs.dimmingOpacity = 0.8;

	// Add the slideshow providing the controlbar and the thumbstrip
	hs.addSlideshow({
		//slideshowGroup: 'group1',
		interval : 5000,
		repeat : false,
		useControls : true,
		fixedControls : 'fit',
		overlayOptions : {
			position : 'bottom center',
			opacity : .75,
			hideOnMouseOut : true
		},
		thumbstrip : {
			position : 'above',
			mode : 'horizontal',
			relativeTo : 'expander'
		}
	});

	// Make all images animate to the one visible thumbnail
	var miniGalleryOptions1 = {
		thumbnailId : 'thumb1'
	}
</script>

<head>

<title><c:out value='${ctx.name}' /> B2B</title>
<%@ include file="/WEB-INF/jspf/head.jspf"%>
<!-- Set Order ID and get Invoice		-->
<c:set target='${infowms}' property='id' value='${param.M_Product_ID}' />
<c:set var='product' value='${infowms.product}' />
<c:if test='${empty product}'>
	<c:set target='${infowms}' property='message' value='product not found' />
	<c:redirect url='products.jsp' />
</c:if>

</head>
<body class="nav-md">
	<div class="container body">
		<div class="main_container">
			<%@ include file="/WEB-INF/jspf/menu.jspf"%>
			<!-- header -->
			<%@ include file="/WEB-INF/jspf/header.jspf"%>

			<script src="//filamentgroup.github.io/demo-head/loadfont.js"></script>
			<!-- page content -->
			<div class="right_col" role="main">
				<div class="">
					<div class="page-title">
						<div class="row">
							<div class="col-md-12 col-sm-12 col-xs-12">
								<div class="x_panel">
									<div class="x_title">
										<div class="title_left">
											<h3>
												<i class="fa fa-cube"></i> Produkty <small></small>
											</h3>
										</div>
										<div class="clearfix"></div>
									</div>
									<div class="x_content">




										<h1>
											Detail Produktu
											<c:out value='${product.name}' />
										</h1>
										<c:if test='${not empty infowms.info}'>
											<p>
												<b><c:out value='${infowms.message}' /></b>
											</p>
										</c:if>


									</div>
								</div>
							</div>
						</div>
						<!--row end -->
					</div>
				</div>

				<div class="clearfix"></div>
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="row">
						<div class="x_panel">
							<div class="x_content">

								<cws:productDetail m_product_ID="${product.m_Product_ID}"
									m_Pricelist_Version_ID="${priceList.priceList_Version_ID}" />

								<h3>detail</h3>
								<div class="alert alert-success" id="success-alert" style="display: none">
		    						<button type="button" class="close" data-dismiss="alert">x</button>
    								<strong>Pridany do kosika! </strong> Produkt bol pridany do kosika.
    							</div>
								<form action="basketServlet" method="post"
									enctype="application/x-www-form-urlencoded" name="products"
									id="products">
									<input name="M_PriceList_ID" type="hidden"
										value="<c:out value='${priceList.priceList_ID}'/>" /> <input
										name="M_PriceList_Version_ID" type="hidden"
										value="<c:out value='${priceList.priceList_Version_ID}'/>" />
									<table class="table table-striped table-bordered ">
										<tr>
											<th colspan="2" align="left">Produkt</th>
											<th>Popis</th>
											<th>Doporucena predajna cena</th>
											<th>Vasa cena</th>
											<th>Balenie</th>
											<th>Stav</th>
											<th>Nahradeny produkt</th>
											<th>Mnozstvo</th>
											<th>UOM</th>
											<th>&nbsp;</th>
											<th>Na sklade</th>
										</tr>
										<c:forEach items='${productDetail.prices}' var='l_product'
											varStatus='status'>
											<jsp:useBean id="status"
												type="javax.servlet.jsp.jstl.core.LoopTagStatus" />
											<c:choose>
												<c:when test="<%=status.getCount() % 2 == 0%>">
													<c:set var="rowClass" value="evenRow" />
												</c:when>
												<c:otherwise>
													<c:set var="rowClass" value="oddRow" />
												</c:otherwise>
											</c:choose>
											<tr>
												<td class="<c:out value='${rowClass}' />"><c:if
														test='${not empty l_product.image()}'>
														<img src="data:image/png;base64,${l_product.image()}" />
													</c:if></td>
												<td class="<c:out value='${rowClass}' />"><input
													name="Name_<c:out value='${l_product.id}'/>" type="hidden"
													value="<c:out value='${l_product.name}'/>" /> <a><c:out
															value="${l_product.name}" /></a></td>
												<td class="<c:out value='${rowClass}' />"><c:out
														value='${l_product.description}' /> <c:if
														test="${empty l_product.description}">&nbsp;</c:if></td>
												<td class="<c:out value='${rowClass}' /> amount"><c:out
														value='${l_product.priceList}' /> EUR</td>
												<td class="<c:out value='${rowClass}' /> amount"><input
													name="Price_<c:out value='${l_product.id}'/>" type="hidden"
													value="<c:out value='${l_product.priceStd}'/> " /> <fmt:formatNumber
														value='${l_product.priceStd}' type="currency"
														currencySymbol="" /> EUR</td>
												<td class="<c:out value='${rowClass}' /> amount"><input
													name="Price_<c:out value='${l_product.id}'/>" type="hidden"
													value="<c:out value='${l_product.order_Pack}'/>" /> <fmt:formatNumber
														value='${l_product.order_Pack}' /></td>

												<td align="center" class="<c:out value='${rowClass}' />"><c:out
														value='${l_product.status}' />&nbsp;</td>
												<td align="center" class="<c:out value='${rowClass}' />"><c:out
														value='${l_product.related}' />&nbsp;</td>
												<td class="<c:out value='${rowClass}' /> quantity"><input
													name="Qty_<c:out value='${l_product.id}'/>" type="text"
													id="qty_<c:out value='${l_product.id}'/>" value="1"
													size="5" maxlength="5" /></td>
												<td class="<c:out value='${rowClass}' />"><c:out
														value='${l_product.uomName}' />&nbsp;</td>
												<td class="<c:out value='${rowClass}' />"><input
													name="AddDetail_<c:out value='${l_product.id}'/>_<c:out value='${product.m_Product_ID}'/>" type="submit"
													id="AddDetail" class="add" value="Add" onClick="show_allert_add('<c:out value='${l_product.name}'/>')" />
													
													</td>
												<td class="<c:out value='${rowClass}'/> quantity"><c:out
														value='${l_product.available}' /> &nbsp;</td>
											</tr>
										</c:forEach>
									</table>

									<p>&nbsp;</p>
									<h3>poznamka k produktu na objednavku</h3>

									<textarea name="Description" id="Description" cols="80"
										rows="5"></textarea>

									<div class="highslide-gallery">
									


										<c:if test='${not empty product.imageBase64_1}'>
											<a class='highslide' id="thumb1"
												href="data:image/png;base64,${product.imageBase64_1}"
												title="${product.name}1"
												onclick="return hs.expand(this, miniGalleryOptions1)"> <img
												src="data:image/png;base64,${product.thumbnailBase64_1}"
												alt='' />
											</a>
										</c:if>
										<c:if test='${not empty product.imageBase64_2}'>

											<a class='highslide' id="thumb2"
												href="data:image/png;base64,${product.imageBase64_2}"
												title="${product.name}2"
												onclick="return hs.expand(this, miniGalleryOptions1)"> <img
												src="data:image/png;base64,${product.thumbnailBase64_2}"
												alt='' /></a>
										</c:if>
										<c:if test='${not empty product.imageBase64_3}'>

											<a class='highslide' id="thumb3"
												href="data:image/png;base64,${product.imageBase64_3}"
												title="${product.name}3"
												onclick="return hs.expand(this, miniGalleryOptions1)"> <img
												src="data:image/png;base64,${product.thumbnailBase64_3}"
												alt='' /></a>
										</c:if>
										<c:if test='${not empty product.imageBase64_4}'>

											<a class='highslide' id="thumb4"
												href="data:image/png;base64,${product.imageBase64_4}"
												title="${product.name}4"
												onclick="return hs.expand(this, miniGalleryOptions1)"> <img
												src="data:image/png;base64,${product.thumbnailBase64_4}"
												alt='' /></a>
										</c:if>
										<c:if test='${not empty product.imageBase64_5}'>

											<a class='highslide' id="thumb5"
												href="data:image/png;base64,${product.imageBase64_5}"
												title="${product.name}5"
												onclick="return hs.expand(this, miniGalleryOptions1)"> <img
												src="data:image/png;base64,${product.thumbnailBase64_5}"
												alt='' /></a>
										</c:if>
										<c:if test='${not empty product.imageBase64_6}'>
											<a class='highslide' id="thumb6"
												href="data:image/png;base64,${product.imageBase64_6}"
												title="${product.name}6"
												onclick="return hs.expand(this, miniGalleryOptions1)"> <img
												src="data:image/png;base64,${product.thumbnailBase64_6}"
												alt='' /></a>
										</c:if>
									</div>
									<!--
	5 (optional). This is how you mark up the caption. The correct class name is important.
-->

									<p>&nbsp;</p>

									<table 	class="table table-striped table-bordered ">
										<tr>
											<th>Kod</th>
											<th>Nazov</th>
											<th>Popis</th>
										</tr>
										<tr>
											<td class="oddRow"><c:out value='${product.value}' /></td>
											<td class="oddRow"><c:out value='${product.name}' />&nbsp;</td>
											<td class="oddRow"><c:out
													value='${product.productDescription}' escapeXml="false" /></td>
											</td>
										</tr>
									</table>

								</form>
								<!-- order lines -->

								<form method="post">

									<h3>Historia objednavok produktu</h3>
									<table 	class="table table-striped table-bordered ">
										<tr>
											<th></th>
											<th>Kod tovaru</th>
											<th>Datum objednania</th>
											<th>Objednavka</th>
											<th>Riadok</th>
											<th>Objednane</th>
											<th>Dodane</th>
											<th>Fakturovane</th>
											<th>Cena</th>
											<th>Dodaci list</th>
											<th>Faktura</th>
										</tr>
										<c:forEach items='${infowms.orderLines}' var='line'
											varStatus='orderstatus'>
											<jsp:useBean id="orderstatus"
												type="javax.servlet.jsp.jstl.core.LoopTagStatus" />
											<c:choose>
												<c:when test="<%=orderstatus.getCount() % 2 == 0%>">
													<c:set var="rowClass" value="evenRow" />
												</c:when>
												<c:otherwise>
													<c:set var="rowClass" value="oddRow" />
												</c:otherwise>
											</c:choose>
											<tr>
												<td align="center"><a data-toggle="modal" class="open-AddSerNoDialog btn btn-default" 
															name="regbutton" value="${line.c_OrderLine_ID}"
															data-id="${line.c_OrderLine_ID}" href="#myModal">reklamuj</button>
															</td>
												<td class="<c:out value='${rowClass}' />"><c:out
														value='${line.productValue}' /></td>
												<td class="<c:out value='${rowClass}' />"><fmt:formatDate
														value='${line.dateOrdered}' /></td>
												<td class="<c:out value='${rowClass}' />"><c:out
														value='${line.docNo}' /></td>
												<td class="<c:out value='${rowClass}' />"><c:out
														value='${line.line}' /></td>
												<td class="<c:out value='${rowClass}' /> quantity"><fmt:formatNumber
														value='${line.qtyOrdered}' /></td>
												<td class="<c:out value='${rowClass}' /> quantity"><fmt:formatNumber
														value='${line.qtyDelivered}' /></td>
												<td class="<c:out value='${rowClass}' /> quantity"><fmt:formatNumber
														value='${line.qtyInvoiced}' /></td>
												<td class="<c:out value='${rowClass}' /> amount"><fmt:formatNumber
														value='${line.priceActual}' type="currency"
														currencySymbol="" /></td>
												<td class="<c:out value='${rowClass}' />"><c:out
														value='${line.shipmentsDocNo}' />&nbsp;</td>
												<td class="<c:out value='${rowClass}' />"><c:out
														value='${line.invoicesDocNo}' />&nbsp;</td>
											</tr>
										</c:forEach>
									</table>
									<p>&nbsp;</p>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
<!-- Modal -->
			<div class="container">

				
				<div class="modal fade" id="myModal" role="dialog">
					<div class="modal-dialog">

						<!-- Modal content-->
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h4 class="modal-title">Poziadavka na reklamaciu</h4>

							</div>
							<div class="modal-body">
								<div class="contact-clean">
									<form id="emailForm" method="post" action="uploadReturnServlet"
										enctype="multipart/form-data">
										<h3>Reklamacia produktu</h3>

											<div class="form-group">
																	<input class="form-control" type="text" name="OrderlineID" value="" id="OrderlineID" value=""  style="visibility: hidden" required="required" readonly/>
											</div>
										<script>
											$(document).on("click", ".open-AddSerNoDialog", function () {
											 var OrderlineID = $(this).data('id');
											 $(".modal-body #OrderlineID").val( OrderlineID );
											});
										</script>
										<div class="form-group">
											<link rel="stylesheet"
												href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
											<script src="//code.jquery.com/jquery-1.10.2.js"></script>
											<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
											<script>
												$(function() {
													// Find any date inputs and override their functionality
													$('input[type="date"]')
															.datepicker(
																	{
																		dateFormat : 'yy-mm-dd'
																	});
												});
											</script>
											Datum prijatia reklamacie u dealera: <input type="date"
												class="form-control" name="ReturnDate" />
										</div>

										<div class="form-group">
											<textarea class="form-control" rows="6" name="Message"
												placeholder="Popis reklamacie"></textarea>
										</div>
										<div class="form-group">
											<h4>Prosim prilozte fotodokumentaciu poskodenia a
												predajny blocek!</h4>
											<input type="file" name="dataFile" id="fileChooser" required
												multiple>
										</div>

										<button class="btn btn-primary" type="submit">Odoslat
											ziadost</button>
									</form>
								<button type="button" class="btn btn-default" data-dismiss="modal">Zavriet</button>
								</div>

							</div>
						</div>

						<div class="modal-footer">

							
						</div>
					</div>

				</div>
			</div>

<!--  Modal up to here -->

			<!-- /page content -->
			<%@ include file="/WEB-INF/jspf/footer.jspf"%>
		</div>
	</div>
	<%@ include file="/WEB-INF/jspf/footnx.jspf"%>

	<script type="text/javascript">
		$(document)
				.ready(
						function() {
							$('#ProductList')
									.DataTable(
											{
												stateSave : true,
												"lengthMenu" : [
														[ 10, 25, 50, 100, -1 ],
														[ 10, 25, 50, 100,
																"All" ] ],
												"language" : {
													"url" : "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Slovak.json"
												}

											});
						});
		
		function show_allert_add(product_name)
		{
			  $("#success-alert").hide();
				$("#success-alert").alert();
	                 $("#success-alert").fadeTo(2000, 500).slideUp(500, function(){
	                $("#success-alert").slideUp(500);
	            });
		}
	</script>
</body>
</html>
