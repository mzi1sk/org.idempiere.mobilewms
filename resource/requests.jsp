<%@ include file="/WEB-INF/jspf/page.jspf" %>
<%@ include file="/WEB-INF/jspf/page.jspf" %>
<c:if test='${empty webUser || !webUser.loggedIn}'>
  <c:redirect url='loginServlet?ForwardTo=login.jsp'/>
</c:if>
<html>
<html lang="en">
<%@ include file="/WEB-INF/jspf/head.jspf" %>
<title><c:out value='${ctx.name}'/> - Poziadavky</title>
</head>
<body class="nav-md">
    <div class="container body">
        <div class="main_container">
            <%@ include file="/WEB-INF/jspf/menu.jspf" %>
                <!-- header -->
                <%@ include file="/WEB-INF/jspf/header.jspf" %>

                    <!-- page content -->
                    <div class="right_col" role="main">
                        <div class="row">

                            <div class="col-md-12 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h2>Poziadavky</h2>
<!--                                         <a href="request.jsp"> -->
<!--                                             <button type="submit" class="btn btn-success" style="float: right;"> New request</button> -->
<!--                                         </a> -->
                                        <div class="clearfix"></div>

                                    </div>
                                    <div class="x_content">
                                        <form class="form-horizontal form-label-left" action="requestsServlet" method="post" enctype="application/x-www-form-urlencoded" name="search" id="search">

                                            <div class="form-group col-md-4 col-sm-4 col-xs-12">

<!--                                                 <label class="control-label">Typ</label> -->
                                                <div class="">
                                                    <select class="form-control" name="RequestType_ID" id="RequestType_ID">
														<option value="1000010" >Dodacie listy</option>
														<option value="1000009" selected>Prijemky</option>
														<option value="1000009" selected>Skladovy presun</option>
														<option value="1000009" selected>Inventura</option>
                                                    </select>
                                                </div>


                                            </div>

<!--                                             <div class="form-group col-md-3 col-sm-4 col-xs-12"> -->


<!--                                                 <label class="control-label">Status</label> -->
<!--                                                 <div class=""> -->
<!--                                                     <select name="r_status" id="r_status" class="form-control"> -->
<!-- 														<option value="Opened">Otvorena</option> -->
<!-- 														<option value="Closed">Ukoncena</option> -->
<!-- 														<option value=""></option> -->
<!--                                                     </select> -->
<!--                                                 </div> -->
<!--                                                  <label class="control-label">Text v sprave</label> -->
<!--                                                 <div class=""> -->
<!--                                                     <input type="text" name="ID_Summary" id="ID_Summary"  class="form-control" /> -->
<!--                                                 </div> -->

<!--                                             </div> -->

                                            <div class="form-group col-md-4 col-sm-4 col-xs-12">


<!--                                                 <label class="control-label">Datum od</label> -->
<!--                                                 <div class="" style="margin-bottom: -30px;"> -->
<!--                                                     <div class="input-group date form_date col-md-12" data-date="" data-date-format="dd MM yyyy" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd"> -->
<!--                                                         <input name="datefrom" id="datefrom"  class="form-control" size="16" type="text" value="" readonly style="background-color: #fff;"> -->
<!--                                                         <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span> -->
<!--                                                         <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span> -->
<!--                                                     </div> -->
<!--                                                     <input type="hidden" id="dtp_input2" value="" /> -->
<!--                                                     <br/> -->
<!--                                                 </div> -->

<!--                                                 <label class="control-label">Datum do</label> -->
<!--                                                 <div class="" style="margin-bottom: -30px;"> -->
<!--                                                     <div class="input-group date form_date col-md-12"  data-date="" data-date-format="dd MM yyyy" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd"> -->
<!--                                                         <input class="form-control" size="16" type="text" name="dateto" id="dateto" value="" readonly style="background-color: #fff;"> -->
<!--                                                         <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span> -->
<!--                                                         <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span> -->
<!--                                                     </div> -->
<!--                                                     <input type="hidden" id="dtp_input2" value="" /> -->
<!--                                                     <br/> -->
<!--                                                 </div> -->

                                            </div>

                                    </div>

                                    
                                    <div class="form-group">
                                        <div class="col-md-12">
<!--                                             <button type="button" class="btn btn-default" name="Reset" value="Reset">Znuluj filter</button> -->
                                            <button type="submit" class="btn btn-default" name="Search" value="Search Requests">Hladaj</button>
                                        </div>
                                        
                                    </div>

                                    </form>
                                </div>
                            </div>
                        </div>

                        <!-- page content -->
                        <div class="row">

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel">

                                    <div class="x_title">
                                        <div class="title_left">
<!--                                             <h3>Request detail</h3> -->
                                        </div>
                                        <div class="clearfix">

                                            <table id="rq_datatable" class="table table-striped table-bordered table-responsive">
                                                <thead>
						                             <tr> 
											          <th>Typ poziadavky</th>	
											          <th>Cislo dokladu</th>
											          <th>Popis</th>
											          <th>Stav</th>
											          <th>Priradena</th>
											          <th>Vytvorena</th>
											       	</tr>
				                                </thead>
				                                <tbody>

                                                <c:forEach items='${info.requestsOwn}' var='request' >
                                                   <tr>
											          <td class="<c:out value='${rowClass}' />"><c:out value='${request.requestTypeName}'/></td>
											          <td class="<c:out value='${rowClass}' />">
												          <c:choose>
												            	   <c:when test="${request.requestType_ID==1000008}">
												            	     <a href="orderDetails.jsp?C_Order_ID=<c:out value='${request.record_ID}'/>">
														                                                <c:out value='${request.orderNo}' />
														                                            </a>
												            	   
												                   </c:when>
												                   <c:otherwise>
												                       <a href="requestDetails.jsp?R_Request_ID=<c:out value='${request.r_Request_ID}'/>">
												                       <c:out value='${request.documentNo}'/></a>
												                   </c:otherwise>
												               </c:choose>
												        </td>
								           
								          		
												          <td class="<c:out value='${rowClass}'  />"   title='${request.summary}' ><c:out value='${request.summaryByType}' escapeXml="false" /></td>
												          <td class="<c:out value='${rowClass}' />"><c:out value='${request.statusName}'/></td>
												          <td class="<c:out value='${rowClass}' />"><c:out value='${request.salesRepName}'/></td>
												          <td class="<c:out value='${rowClass}' />"><fmt:formatDate value='${request.created}'/> <c:out value='${request.createdByName}'/></td>
		                                         </tr>
                                             </c:forEach>

                                        </div>
                                    </div>

                                </div>
                                <!-- End of row -->
                            </div>
                        </div>

                        <!-- /page content -->

                    </div>
        </div>
			<!-- /page content -->
<%-- 			<%@ include file="/WEB-INF/jspf/footer.jspf"%> --%>
		</div>
	</div>
	<%@ include file="/WEB-INF/jspf/footnx.jspf"%>
	<script>
		document.getElementById('r_status').value = '${param.r_status}';
		document.getElementById('RequestType_ID').value = '${param.RequestType_ID}';
		document.getElementById('datefrom').value = '${param.datefrom}';
		document.getElementById('dateto').value = '${param.dateto}';
		document.getElementById('ID_Summary').value = '${param.ID_Summary}';
		
	</script>
</body>
</html>

