<%@ include file="/WEB-INF/jspf/page.jspf" %>
<c:if test='${empty webUser || !webUser.loggedIn}'>
  <c:redirect url='../'/>
</c:if>
<html lang="en">
<%@ include file="/WEB-INF/jspf/head.jspf" %>
<title><c:out value='${ctx.name}'/> - My Shipments</title>
</head>
<body class="nav-md">
    <div class="container body">
      <div class="main_container">
<%@ include file="/WEB-INF/jspf/menu.jspf" %>
<!-- header -->
<%@ include file="/WEB-INF/jspf/header.jspf" %>
        <div class="right_col" role="main">
		    <div class="">
		        <div class="page-title">
		            <div class="row">
		                <div class="col-md-12 col-sm-12 col-xs-12">
		                    <div class="x_panel">
		                        <div class="x_title">
		                            <div class="title_left">
		                            <a href="shipments.jsp">
		                                <h3><i class="fa fa-truck"></i> Dodacie listy <small> na spracovanie</small></h3>
		                            </div>
		                            <div class="clearfix"></div>
		                        </div>
		                    </div>
		                </div>
		            </div>
		        </div>
		    </div>
		    <div class="clearfix"></div>


            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_content">
      			<form action="basketServlet" method="post" enctype="application/x-www-form-urlencoded" name="products" id="products">
        			<input name="M_PriceList_ID" type="hidden" value="<c:out value='${priceList.priceList_ID}'/>" />
        			<input name="M_PriceList_Version_ID" type="hidden" value="<c:out value='${priceList.priceList_Version_ID}'/>" />

                    <table id="CustomerShipments" class="table table-striped table-bordered">
                      <thead>
				          <tr> 
					          <th>Dodaci list</th>
					          <th>Objednavka</th>
					          <th>Partner</th>
<!-- 					          <th>Stav</th> -->
				          </tr>
                      </thead>
	                      <tbody>
					        <c:forEach items='${infowms.shipmentsOutbound}' var='shipment' varStatus='status'>
					        	<jsp:useBean id="status" type="javax.servlet.jsp.jstl.core.LoopTagStatus" />
					        	<c:choose>
					        		<c:when test="<%= status.getCount() %2 == 0 %>">
						        		<c:set var="rowClass" value="evenRow"/>
					        		</c:when>
					        		<c:otherwise>
						        		<c:set var="rowClass" value="oddRow"/>
					        		</c:otherwise>
					        	</c:choose> 
					        <tr> 
					          <td class="<c:out value='${rowClass}' />"><a href="shipmentDetails.jsp?M_InOut_ID=<c:out value='${shipment.m_InOut_ID}'/>"><c:out value='${shipment.documentNo}'/></a></td>
					          <td class="<c:out value='${rowClass}' />"><a href="shipmentDetails.jsp?M_InOut_ID=<c:out value='${shipment.m_InOut_ID}'/>"><c:out value='${shipment.orderNo}'/></a></td>
					          <td class="<c:out value='${rowClass}' />"><a href="shipmentDetails.jsp?M_InOut_ID=<c:out value='${shipment.m_InOut_ID}'/>"><c:out value='${shipment.BPName}'/></a></td>
<%-- 					          <td class="<c:out value='${rowClass}' />"><c:out value='${shipment.docStatusName}'/></td> --%>
					          
					        </tr>
					        </c:forEach> 
	                       </tbody>
	                   </table>
                   	</form>
                  </div>
                </div>
              </div>
            </div>
          </div>
			<!-- /page content -->
			<%@ include file="/WEB-INF/jspf/footer.jspf"%>
		</div>
	</div>
	<%@ include file="/WEB-INF/jspf/footnx.jspf"%>
		
</body>
</html>