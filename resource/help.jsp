<%@ include file="/WEB-INF/jspf/page.jspf" %>
<c:if test='${empty webUser || !webUser.loggedIn}'>
  <c:redirect url='loginServlet?ForwardTo=orders.jsp'/>
</c:if>
<html>
<html lang="en">
<%@ include file="/WEB-INF/jspf/head.jspf" %>
<title><c:out value='${ctx.name}'/> - Help</title>
</head>
<body class="nav-md">
    <div class="container body">
      <div class="main_container">
<%@ include file="/WEB-INF/jspf/menu.jspf" %>
<!-- header -->
<%@ include file="/WEB-INF/jspf/header.jspf" %>
<!-- page content -->
      <div class="right_col" role="main">
		    <div class="">
		        <div class="page-title">
		            <div class="row">
		            <div class="x_panel">
						<h1>Pomoc</h1>
						</br>
							<h3>Sklad</h3>
								<p>Prehlad skladu Expedície. </p>
							<h3>Dodacie listy</h3>
								<p>Zoznam dodacich listov pripravenych na naskladnenie paliet. </p>
							<h3>Prijemky</h3>
								<p>
								
								
								.</p>
							<h3>Inventura</h3>
								<p>
								
								
								.</p>
							<h3>Poziadavky</h3>
								<p>
								
								.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
			<!-- /page content -->
			<%@ include file="/WEB-INF/jspf/footer.jspf"%>
		</div>
	</div>
	<%@ include file="/WEB-INF/jspf/foot.jspf"%>
</body>
</html>
