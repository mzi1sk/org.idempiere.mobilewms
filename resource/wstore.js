/****************************************************************************
 * Web Store Scripts
 ***************************************************************************/

var mandatory = "Enter mandatory:";

/**
 *	Is field empty ?
 *  Returns true if field is empty
 */
function isEmpty (value)
{
	if (value == null)
		return true;
	if (value == "")
		return true;
	for (var i = 0; i < value.length; i++)
	{
		var c = value.charAt(i);
		if ((c != ' ' && c != '\n' && c != '\t'))
			return false;
	}
	return true;
}

function changeBike()
{
	document.getElementById('SubmitMotocycle').click();
}




function changeCountry(countrySelectId)
{
    var countrySelect = document.getElementById(countrySelectId);
    var countryOption = countrySelect.options[countrySelect.selectedIndex];
    var countryId = countryOption.attributes.getNamedItem('value').value;

    /**
     * call locationServlet
     * param: cmd = 'regions'
     * param: country = countryId
     *
     * get back:
     *  <regions country='countryId'>
     *      <region id='regionID'>AL</region>
     *      <region id='regionID'>AK</region>
     *      <region id='regionID' selected='true'>OR</region>
     *  </regions>
     */

   var params = new Array();
    params['cmd']='regions';
    params['country']=countryId;
    //params['selected']=regionId;
    var loader = new AJAX.AjaxLoader("locationServlet", changeCountryCallback, null, "GET", params);
}

function changeCountryCallback()
{
	updateSelect(this, "regions", "ID_C_Region_ID");
}



