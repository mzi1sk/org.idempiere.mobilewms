<%@ include file="/WEB-INF/jspf/page.jspf"%>
<c:if test='${empty webUser || !webUser.loggedIn}'>
	<c:redirect url='loginServlet?ForwardTo=shipments.jsp' />
</c:if>
<html>
<!--

  -->
<head>
<%@ include file="/WEB-INF/jspf/head.jspf"%>
<title><c:out value='${ctx.name}' /> - Dodaci List</title>
<style>
/* Have to place it here as Android Chrome has bug that is ignorig css in some usecases with backround tags if not placed in header */
.error_msg_box2 {
	width: 200px;
	height: 20px;
	height: auto;
	position: absolute;
	left: 50%;
	margin-left: -100px;
	bottom: 10px;
	background-color: #383838;
	color: #F0F0F0;
	font-family: Calibri;
	font-size: 20px;
	padding: 10px;
	text-align: center;
	border-radius: 2px;
	-webkit-box-shadow: 0px 0px 24px -1px rgba(56, 56, 56, 1);
	-moz-box-shadow: 0px 0px 24px -1px rgba(56, 56, 56, 1);
	box-shadow: 0px 0px 24px -1px rgba(56, 56, 56, 1);
}
</style>

</head>

<body class="nav-md" onload="initfocus()">


	<div class="container body">
		<div class="main_container">
		<div class='error_msg_box2' style='display: none'
							id="error_message" name="error_message">I did something!</div>
			<%@ include file="/WEB-INF/jspf/menu.jspf"%>
			<!-- header -->
			<%@ include file="/WEB-INF/jspf/header.jspf"%>
			<!-- Set Order ID and get shipment		-->
			<c:set target='${infowms}' property='id' value='${param.M_InOut_ID}' />
			<c:set var='shipment' value='${infowms.shipment}' />
			<c:if test='${empty shipment}'>
				<c:set target='${infowms}' property='message'
					value='shipment not found' />
				<c:redirect url='shipments.jsp' />
			</c:if>
			<div class="right_col" role="main">
				<div class="">
					<div class="page-title">
						<div class="row">

							<div class="col-md-12 col-sm-12 col-xs-12">
								<div class="x_panel">
									<div class="x_title">
										<div class="title_left">
											<a href="shipmentDetails.jsp?M_InOut_ID=<c:out value='${shipment.m_InOut_ID}'/>">
											<h3>
												<i class="fa fa-truck"></i><small> Riadky DL </small>
												<c:out value='${shipment.documentNo}' />

											</h3>
										</div>

									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="clearfix"></div>
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="x_panel">
								<div class="x_content">
									<div id="content">
										<table class="table table-striped table-bordered">
											<thead>
												<tr>
													<th>r.</th>
													<th>Popis</th>
													<th>Pocet</th>
													<th>Naskl.</th>
												</tr>
											</thead>
											<tbody>
												<c:forEach items='${shipment.lines}' var='line'
													varStatus='status'>
													<jsp:useBean id="status"
														type="javax.servlet.jsp.jstl.core.LoopTagStatus" />
													<c:choose>
														<c:when test="<%=status.getCount() % 2 == 0%>">
															<c:set var="rowClass" value="evenRow" />
														</c:when>
														<c:otherwise>
															<c:set var="rowClass" value="oddRow" />
														</c:otherwise>
													</c:choose>

													<tr>

														<td class="<c:out value='${rowClass}' />"><a
															data-toggle="modal" class="open-AddSerNoDialog"
															onclick="loadpallets('${line.lotNumbers}')"
															name="regbutton" value="${line.m_InOutLine_ID}"
															data-id="${line.m_InOutLine_ID}"
															data-model="${line.qtyEntered}_ks_${line.productValue}"
															href="#myModal"><c:out value='${line.line}' /> </a></td>
														<td class="<c:out value='${rowClass}' />"><a
															data-toggle="modal" class="open-AddSerNoDialog"
															name="regbutton" value="${line.m_InOutLine_ID}"
															onclick="loadpallets('${line.lotNumbers}')"
															data-id="${line.m_InOutLine_ID}"
															data-model="${line.qtyEntered}_ks_${line.productValue}"
															href="#myModal"><c:out value='${line.productValue}' />
														</a> &nbsp;</td>
														<td class="<c:out value='${rowClass}' /> quantity"><a
															data-toggle="modal"
															onclick="loadpallets('${line.lotNumbers}')"
															name="regbutton" class="open-AddSerNoDialog"
															value="${line.m_InOutLine_ID}"
															data-id="${line.m_InOutLine_ID}"
															data-model="${line.qtyEntered}_ks_${line.productValue}"
															href="#myModal"><fmt:formatNumber
																	value='${line.qtyEntered}' /> </a></td>
														<td class="<c:out value='${rowClass}' />"><a
															data-toggle="modal"
															onclick="loadpallets('${line.lotNumbers}')"
															name="regbutton" class="open-AddSerNoDialog"
															value="${line.m_InOutLine_ID}"
															data-id="${line.m_InOutLine_ID}"
															data-model="${line.qtyEntered}_ks_${line.productValue}"
															href="#myModal"><fmt:formatNumber
																	value='${line.pickedQty}' /> </a>
	
														</td>
													</tr>
												</c:forEach>
											</tbody>
										</table>
										<p>&nbsp;</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<button class="btn btn-primary" onclick="productscan()"
										type="submit" name="submit">potvrdit</button>
				<!-- /page content -->
			</div>
			<%@ include file="/WEB-INF/jspf/footer.jspf"%>

		</div>
	</div>
	<%@ include file="/WEB-INF/jspf/footnx.jspf"%>



	<div class="container">

		<!-- Modal -->
		<div class="modal" id="myModal" role="dialog">
			<div class="modal-dialog">

				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Riadok dokladu</h4>


					</div>

					<div class="modal-body">
						
						<div class="contact-clean">
							<form id="palletForm" method="post" action="shipmentsServlet"
								enctype="multipart/form-data">
								<div class="form-group">
									<input type="hidden" name="pallets" id="pallets" value="">
									<input class="form-control" type="text" name="modelname"
										value="" id="modelname" value="" required="required" readonly />
									<input class="form-control" type="text" name="mionutline_id"
										value="" id="mionutline_id" style="display: none;" value=""
										required="required" readonly />

								</div>
								<script>
										$(document).on("click", ".open-AddSerNoDialog", function () {
											 var modelname = $(this).data('model');
											 $(".modal-body #modelname").val( modelname );
											 var inuotline_id = $(this).data('id');
											 $(".modal-body #mionutline_id").val( inuotline_id );
										});
										
										$('#myModal').on('shown.bs.modal', function () {
										    $('#palletid').focus();
										})  
									</script>

								<textarea id="palletid" class="form-control" rows="1"
									name="palletid" placeholder="Paleta cislo" onfocus="blur();"></textarea>

								<div align="right">
									<button type="button" class="btn btn-danger">
			                            <span class="glyphicon glyphicon-remove"> Zmazat riadok</span>
			                        </button>
									<button type="button" class="btn btn-default"
										data-dismiss="modal" onclick="reloadpage()">Zavriet</button>
								</div>


						</div>


					</div>


					<div class="modal-footer"></div>
				</div>
				<!-- Modal content-->

			</div>
		</div>


	</div>

	<script type="text/javascript">
			$(document).ready(function() {
				$('#ShowroomSR').DataTable( {
				stateSave: true,
				"lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50,100, "All"]]
    			} );
			} );
	
			
			$(document).ready(function() {
		        var barcode="";
		        var counter=0;
		    $(document).keydown(function(e) {

		        var code = (e.keyCode ? e.keyCode : e.which);
		        if(code==13 || code==9)// Enter key hit or Tab key hit
		        {
		        	counter=counter+1;
		        	insRow(document.getElementById('palletid').value);
		        	barcode='';
		        	 document.getElementById('palletid').value = "";
		        }
		        else
		        {
		            barcode=barcode+String.fromCharCode(code);
		            document.getElementById('palletid').value = barcode;
		        }
		    });

		    });
			
			function reloadpage()
			{
				 location.reload();
			}
			
			function loadpallets(palletslist)
			{
				 var x=document.getElementById('POITable');
				 $("#POITable tbody tr").remove();
				 
				 if (palletslist!=""){
					 
				 	var tableRef = document.getElementById('POITable').getElementsByTagName('tbody')[0];
				    var str_array  = palletslist.split(';');
				    
				    for(var i = 0; i < str_array.length; i++) {
				    	var productean=str_array[i];
				    	var len=i+1;
					    if (len==1) {
						    
					    	var new_row   = tableRef.insertRow(tableRef.rows.length);
					    	var newCell0  = new_row.insertCell(0);
						    new_row.cells[0].innerHTML = len;
						    
						    var newCell1  = new_row.insertCell(1);
						    new_row.cells[1].innerHTML = ' <input size=25 type="text" id="lngbox" readonly=true outline="none" />';
						    var inp1 = new_row.cells[1].getElementsByTagName('input')[0];
						    inp1.id += len;
						    inp1.value = productean;
						    
						    var newCell2  = new_row.insertCell(2);
						    new_row.cells[2].innerHTML =  ' <a class="btn btn-default" onclick="deleteRow('+len+')"> <i  class="fa fa-trash-o" aria-hidden="true" style="color:red;" ></i></a>';
						    
						    x.appendChild( new_row );
					    	
					    }
					    else 
					    {
						    var new_row = x.rows[1].cloneNode(true);
						    new_row.cells[0].innerHTML = len+'.';
						    
						    var inp1 = new_row.cells[1].getElementsByTagName('input')[0];
						    new_row.cells[2].innerHTML =  ' <a class="btn btn-default" onclick="deleteRow('+len+')"> <i  class="fa fa-trash-o" aria-hidden="true" style="color:red;" ></i></a>';
							   
						    inp1.id += len;
						    inp1.value = productean;
						    x.appendChild( new_row );
					    }
			    	}
				 }
			}
			
			
			
			function deleteRow(row_id)
			{
			    
				var row=document.getElementById('POITable').rows[row_id];
			    var xhr = new XMLHttpRequest();   	
			    var inp1 = row.cells[1].getElementsByTagName('input')[0];
				var producteantodel=inp1.value;
				
				var parameters="shipmentsServlet?"+"producteantodel="+producteantodel+"&m_inoutline_id="+document.getElementById("mionutline_id").value;
			    xhr.open("GET", parameters, true);
			    xhr.send();

			    document.getElementById('POITable').deleteRow(row_id);
			    
			    renumber_rows();
			}


			function insRow(productean)
			{
			    var xhr = new XMLHttpRequest();
			    xhr.open("GET", "shipmentsServlet?"+"productean="+productean+"&m_inoutline_id="+document.getElementById("mionutline_id").value+"&m_inout_id="+${param.M_InOut_ID}, false);
			    xhr.send();
			    var response=xhr.response;

			    console.log('productean verification : '+response);
			    
			    if (response=="OK")   	{
			    	
			    	 if (document.getElementById("mionutline_id").value=== "") {
					    reloadpage(); 
					    return;
			    	 }
				    var x=document.getElementById('POITable');
				    var tableRef = document.getElementById('POITable').getElementsByTagName('tbody')[0];
				    var len = x.rows.length;
				    if (len==1) {
				    	var new_row   = tableRef.insertRow(tableRef.rows.length);
				    	var newCell0  = new_row.insertCell(0);
					    new_row.cells[0].innerHTML = len;
					    
					    var newCell1  = new_row.insertCell(1);
					    new_row.cells[1].innerHTML = ' <input size=25 type="text" id="lngbox" readonly=true outline="none" />';
					    var inp1 = new_row.cells[1].getElementsByTagName('input')[0];
					    inp1.id += len;
					    inp1.value = productean;
					    
					    var newCell2  = new_row.insertCell(2);
					    new_row.cells[2].innerHTML =  ' <a class="btn btn-default" onclick="deleteRow('+len+')"> <i  class="fa fa-trash-o" aria-hidden="true" style="color:red;" ></i>  	</a>';
					    
					    x.appendChild( new_row );
				    	
				    }
				    else 
				    {
					    var new_row = x.rows[1].cloneNode(true);
					    new_row.cells[0].innerHTML = len+'.';
					    
					    var inp1 = new_row.cells[1].getElementsByTagName('input')[0];
					    inp1.id += len;
					    inp1.value = productean;
					    
					    new_row.cells[2].innerHTML =  ' <a class="btn btn-default" onclick="deleteRow('+len+')"> <i  class="fa fa-trash-o" aria-hidden="true" style="color:red;" ></i>  	</a>';
					    
					    x.appendChild( new_row );
					    
				    }
				    
				   
				    
			    } else
			    {
			    	if (response=="01")
			    		showerrmessage("Produkt nie je na DL!");

			    	if (response=="02")
			    		showerrmessage("Tovar sa nezhoduje s riadkom DL!");
			    		    		
			    	if (response=="03")
			    		showerrmessage("Systemovy problem!");
			    	
			    	if (response=="04")
			    		showerrmessage("Pozor prekroceny pocet kusov tovaru!");
			    	
			    	if (response=="05")
			    		showerrmessage("Pozor prekroceny pocet kusov tovaru!");
			    	 
			    }
			  
			    
			}
			
			
			function showerrmessage(err_message)
			{
				beep();
				document.getElementById("error_message").textContent= err_message;
				$('.error_msg_box2').stop().fadeIn(400).delay(3000).fadeOut(400);
				
			}
			
			function initfocus()
			{
			    document.getElementById("palletid").focus();
			}
			

			 function productscan(){  
 				    var xhr = new XMLHttpRequest();   	
					var parameters="shipmentsServlet?"+"confirmshipment=true&m_inout_id="+${param.M_InOut_ID};
				    xhr.open("GET", parameters, true);
				    xhr.send();
				    showmessage("Spracovavam dodaci list.");
				    window.location.href ="shipments.jsp";
			 }
		
			 function renumber_rows(){
					var oTable = document.getElementById('POITable');
					//gets rows of table
					var rowLength = oTable.rows.length;
					//loops through rows    
					for (i = 1; i < rowLength; i++){
					   //gets cells of current row
					   var row = oTable.rows.item(i);
					   row.cells[0].innerHTML = i+'.';
					   row.cells[2].innerHTML = ' <a class="btn btn-default" onclick="deleteRow('+i+')"> <i  class="fa fa-trash-o" aria-hidden="true" style="color:red;" ></i>  	</a>';
					}
				 
			}
					 
			 function showmessage(message)
			{
				
				document.getElementById("error_message").textContent= message;
				$('.error_msg_box2').stop().fadeIn(400).delay(6000).fadeOut(400);
				
			}
				 

			 function beep() {
				 var audio = new Audio('assets/sounds/alarm.mp3');
				 audio.play();
			}
		</script>
</body>
</html>