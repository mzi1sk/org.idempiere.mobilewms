<%@ include file="/WEB-INF/jspf/page.jspf"%>
<c:if test='${empty webUser || !webUser.loggedIn}'>
	<c:redirect url='loginServlet?ForwardTo=login.jsp' />
</c:if>
<head>

<title><c:out value='${ctx.name}' /> WMS</title>
<%@ include file="/WEB-INF/jspf/head.jspf"%>
</head>
<body class="nav-md">
	<div class="container body">
		<div class="main_container">
			<%@ include file="/WEB-INF/jspf/menu.jspf"%>
			<!-- header -->
			<%@ include file="/WEB-INF/jspf/header.jspf"%>
    <%@ include file="/WEB-INF/jspf/vendor.jspf" %>

			<script src="//filamentgroup.github.io/demo-head/loadfont.js"></script>
			<!-- page content -->
			<div class="right_col" role="main">
				<div class="">
					<div class="page-title">
						<div class="row">
							<div class="col-md-12 col-sm-12 col-xs-12">
								<div class="x_panel">
									<div class="x_title">
										<div class="title_left">
											<h3>
												<i class="fa fa-cube"></i> Sklad Expedicia <small></small>
											</h3>
										</div>
										<div class="clearfix"></div>
									</div>
								</div>
							</div>
						</div>
						<!--row end -->
					</div>
				</div>

				<div class="clearfix"></div>
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="row">
						<div class="x_panel">
							<div class="x_content">
								<form action="basketServlet" method="post"
									enctype="application/x-www-form-urlencoded" name="products"
									id="products">

									<table id="ProductList"
										class="table table-striped table-bordered ">
										<thead>
											<tr>
												<th>Produkt</th>
												<th>Lokacia</th>
												<th>Pocet Paliet</th>
												<th>Na sklade</th>
											</tr>
										</thead>
										<tbody>
											<c:forEach items='${infowms.products}' var='product'>
										
												<tr>

													<td class="<c:out value='${rowClass}' />"><input
														name="Name_<c:out value='${product.m_Product_ID}'/>" type="hidden"
														value="<c:out value='${product.name}'/>" /> <a
														href="productDetails.jsp?M_Product_ID=<c:out value='${product.m_Product_ID}'/>"><c:out
																value="${product.name}" /></a></td>
													<td class="<c:out value='${rowClass}' />"><c:out
															value='${product.locator_position}' />&nbsp;</td>
													<td class="<c:out value='${rowClass}'/> Pallets"><c:out
															value='${product.qtypallete}' /> &nbsp;</td>															
													<td class="<c:out value='${rowClass}'/> quantity"><c:out
															value='${product.qtyOnHand}' /> &nbsp;</td>

												</tr>
											</c:forEach>
										</tbody>
									</table>
								</form>
								<p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- /page content -->
			<%@ include file="/WEB-INF/jspf/footer.jspf"%>
		</div>
	</div>
	<%@ include file="/WEB-INF/jspf/footnx.jspf"%>

	<script type="text/javascript">
		$(document)
				.ready(
						function() {
							$('#ProductList')
									.DataTable(
											{
												stateSave : true,
												"lengthMenu" : [
														[ 10, 25, 50, 100, -1 ],
														[ 10, 25, 50, 100,
																"All" ] ],
												"language" : {
													"url" : "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Slovak.json"
												}

											});
						});
	</script>
</body>
</html>