/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2006 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package org.idempiere.mims.servlet;

import java.util.Properties;
import java.util.logging.Level;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.ecs.xhtml.a;
import org.apache.ecs.xhtml.br;
import org.apache.ecs.xhtml.hr;
import org.apache.ecs.xhtml.i;
import org.apache.ecs.xhtml.span;
import org.apache.ecs.xhtml.ul;
import org.apache.ecs.xhtml.li;
import org.compiere.model.MBPartner;
import org.compiere.model.MStore;
import org.compiere.util.CLogger;
import org.compiere.util.HtmlCode;
import org.compiere.util.WebSessionCtx;
import org.idempiere.mims.WebInfoIMS;
import org.idempiere.mims.WebUserIMS;

/**
 *  Info Links (Menu).
 *  <cws:infoLink/>
 */
public class InfoLinkTag extends TagSupport
{
	/** SV */
	private static final long serialVersionUID = 7608741032814139346L;
	/**	Logger	*/
	private static CLogger log = CLogger.getCLogger (InfoLinkTag.class);
	/** One Line						*/
	private boolean			m_oneLine = false;

	/**
	 *	Set to one line
	 *	@param var Y or something else
	 */
	public void setOneLine (String var)
	{
		m_oneLine = "Y".equals(var);
	}	//	setOneLine

	/**
	 *  Start Tag
	 *  @return SKIP_BODY
	 * 	@throws JspException
	 */
	public int doStartTag() throws JspException
	{
		HttpServletRequest request = (HttpServletRequest)pageContext.getRequest();
		Properties ctx = JSPEnv.getCtx(request);		//	creates wsc/wu
		WebSessionCtx wsc = WebSessionCtx.get(request);
		//
		HttpSession session = pageContext.getSession();
		WebUserIMS wu = (WebUserIMS)session.getAttribute(WebUserIMS.NAME);
		if (wu != null && wu.isLoggedIn())
		{
			if (ctx != null)
			{
				WebInfoIMS info = (WebInfoIMS)session.getAttribute(WebInfoIMS.NAME);
				if (info == null || wu.getAD_User_ID() != info.getAD_User_ID())
				{
					info = new WebInfoIMS (ctx, wu);
					session.setAttribute (WebInfoIMS.NAME, info);
				}
			}
			//
		//	log.fine("WebUser exists - " + wu);
			//
			JspWriter out = pageContext.getOut();
			HtmlCode html = new HtmlCode();

	
			html.output(out);
		}
		else
		{
			if (log.isLoggable(Level.FINE)) 
				log.fine("No WebUser");
			if (session.getAttribute(WebInfoIMS.NAME) == null)
				session.setAttribute (WebInfoIMS.NAME, WebInfoIMS.getGeneral());
		}
		return (SKIP_BODY);
	}   //  doStartTag


		/**
	 * 	Add New Line / Break
	 * 	@param html code
	 * 	@param hr insert HR rather BR
	 */
	private void nl (HtmlCode html, boolean hr)
	{
		if (m_oneLine)
			html.addElement("&nbsp;- ");
		else if (hr)
			html.addElement(new hr("90%", "left"));
		else
			html.addElement(new br());
	}	//	nl

	/**
	 * 	End Tag
	 * 	@return EVAL_PAGE
	 * 	@throws JspException
	 */
	public int doEndTag() throws JspException
	{
		return EVAL_PAGE;
	}	//	doEndTag

}	//	InfoLinkTag
