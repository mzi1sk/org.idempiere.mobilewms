/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2006 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package org.idempiere.mims.servlet;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Properties;
import java.util.logging.Level;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.compiere.model.I_M_PriceList;
import org.compiere.model.MPriceListVersion;
import org.compiere.model.MProductCategory;
import org.compiere.model.MProductPricing;
import org.compiere.util.CCache;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.WebSessionCtx;
import org.idempiere.mims.WebUserIMS;


/**
 */
public class ProductDetail {
	/**
	 * Get Price List
	 *
	 * @param AD_Client_ID
	 *            client
	 * @param M_PriceList_ID
	 *            price list
	 * @param searchString
	 *            query search string
	 * @param productCategory
	 *            query product category string
	 * @param allRecords
	 *            all if no query
	 * @return Price list
	 */
	public static ProductDetail get(Properties ctx, int AD_Client_ID,
			int M_Product_ID,int m_Pricelist_Version_ID, String searchString, String productCategory,
			boolean allRecords,HttpServletRequest request) {
		// Search Parameter
		String search = searchString;
		if (search != null && (search.length() == 0 || search.equals("%")))
			search = null;
		if (search != null) {
			if (!search.endsWith("%"))
				search += "%";
			if (!search.startsWith("%"))
				search = "%" + search;
			search = search.toUpperCase();
		}
		int M_Product_Category_ID = 0;
		try {
			if (productCategory != null && productCategory.length() > 0)
				M_Product_Category_ID = Integer.parseInt(productCategory);
		} catch (Exception e) {
		}
		if (M_Product_Category_ID < 0)
			M_Product_Category_ID = 0;

		// Search Price List Cache
		String key = String.valueOf(AD_Client_ID) + "_" + M_Product_ID;
		ProductDetail retValue = null;
		if (search == null && M_Product_Category_ID == 0 && allRecords)
			retValue = s_cache.get(key);

		// create New
		if (retValue == null) {
			retValue = new ProductDetail(ctx, AD_Client_ID, M_Product_ID,m_Pricelist_Version_ID, search,
					M_Product_Category_ID, allRecords, request);
			if (search == null && M_Product_Category_ID == 0 && allRecords)
				s_cache.put(key, retValue);
		}
		return retValue;
	} // get

	/** Price List Cache */
	private static CCache<String, ProductDetail> s_cache = new CCache<String, ProductDetail>(
			I_M_PriceList.Table_Name, "PriceList", 5, 60, true); // 1h Cache
	/** Maximum Lines to be displayed */
	public static int MAX_LINES = 50;

	/*************************************************************************
	 * PriceList constructor.
	 *
	 * @param ctx
	 *            context
	 * @param AD_Client_ID
	 *            client
	 * @param M_PriceList_ID
	 *            optional price list
	 * @param searchString
	 *            query search string
	 * @param M_Product_Category_ID
	 *            query product category
	 * @param allRecords
	 *            all if no query
	 */
	private ProductDetail(Properties ctx, int AD_Client_ID, int M_Product_ID, int m_Pricelist_Version_ID,
			String searchString, int M_Product_Category_ID, boolean allRecords,HttpServletRequest request) {
		if (log.isLoggable(Level.FINER))
			log.finer("AD_Client_ID=" + AD_Client_ID + ", M_Product_ID="
					+ M_Product_ID + ", Search=" + searchString
					+ ",M_Product_Category_ID=" + M_Product_Category_ID
					+ ", All=" + allRecords);
		m_ctx = ctx;


		loadProducts(AD_Client_ID, searchString, M_Product_ID, m_Pricelist_Version_ID,
				allRecords, request);
	} // PriceList

	/** Attribute Name - also in JSPs */
	public static final String NAME = "productDetail";
	/** Logging */
	private CLogger log = CLogger.getCLogger(getClass());

	private String m_name = "Not found";
	private String m_description;
	private String m_currency;
	private String m_curSymbol;
	private String m_AD_Language;
	private boolean m_taxIncluded;
	private String m_searchInfo = "";
	private boolean m_notAllPrices = false;

	/** Price Lines */

	/** Context */
	private Properties m_ctx;

	
	/**
	 * Load From Product Price
	 *
	 * @param searchString
	 *            query search string
	 * @param M_Product_Category_ID
	 *            query product category
	 * @param allRecords
	 *            all only true if called from BasketServlet
	 */
	private void loadProducts(int AD_Client_ID, String searchString,
			int M_Product_ID,int m_Pricelist_Version_ID, boolean allRecords,HttpServletRequest request) {
		// Set Search String
		if (log.isLoggable(Level.FINER))
			log.finer("loadProducts - M_PriceList_Version_ID="
					+ m_Pricelist_Version_ID + ", Search=" + searchString
					+ ", M_Product_ID=" + M_Product_ID);
	
		
		// get user 
		Properties ctx = JSPEnv.getCtx(request);
		HttpSession session = request.getSession(true);
		session.removeAttribute(WebSessionCtx.HDR_MESSAGE);

		WebUserIMS wu = (WebUserIMS)session.getAttribute(WebUserIMS.NAME);
		
		
		m_notAllPrices = false;
		//
		String sql = "SELECT p.M_Product_ID, p.Value, p.Value, p.Name, " // 1..4
				+ "p.Help, p.DocumentNote, p.ImageURL, p.DescriptionURL, " // 5..8
				+ "pp.PriceStd, uom.Name, uom.UOMSymbol, pp.PriceList, " // 9..12
				+ "po.Order_Pack,Case when p.discontinued='Y' then 'I' else 'C' end as stav,ps.value ," // 13..15
				+ " COALESCE(st.QtyOnHand,0)" // 16
				+ "FROM M_ProductPrice pp "
				+ " INNER JOIN M_Product p ON (pp.M_Product_ID=p.M_Product_ID AND p.IsActive='Y' AND p.IsSold='Y')"
				+ " INNER JOIN M_Product_PO po ON ( p.M_Product_ID = po.M_Product_ID and po.iscurrentvendor='Y' and po.IsActive='Y')"
				+ " INNER JOIN C_UOM uom ON (p.C_UOM_ID=uom.C_UOM_ID) "
				+ " LEFT JOIN m_substitute s ON (s.M_Product_ID=p.M_Product_ID)"
				+ " LEFT JOIN m_product ps ON (ps.M_product_ID=s.substitute_id)"
				+ " LEFT JOIN (SELECT SUM(QtyOnHand) AS QtyOnHand,"
				+ " SUM(QtyOrdered) AS QtyOrdered,"
				+ " SUM(QtyReserved) AS QtyReserved,"
				+ " M_Product_ID"
				+ " FROM M_Storage_By_Product WHERE AD_Client_ID=?"
				+ " GROUP BY M_Product_ID) "
				+ " st ON (p.M_Product_ID=st.M_Product_ID) "
				+ "WHERE  pp.M_Pricelist_Version_ID=? AND pp.PriceStd > 0 " + " "
				; //AND p.IsSelfService='Y'
		sql += " AND  ( p.m_product_id in (select rel.M_Product_ID from M_RelatedProduct rel where rel.ad_client_id=? AND rel.relatedproduct_id=?) OR p.M_Product_ID=?) "
				+ " AND p.issummary='N' ";
		sql += "ORDER BY p.M_Product_Category_ID, p.Value ";
		// log.fine("loadProducts - " + sql);

		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = DB.prepareStatement(sql, null);
			pstmt.setMaxRows(10);
			int index = 1;
			pstmt.setInt(index++,AD_Client_ID );
			pstmt.setInt(index++,m_Pricelist_Version_ID );
			pstmt.setInt(index++,AD_Client_ID );
			pstmt.setInt(index++, M_Product_ID);
			pstmt.setInt(index++, M_Product_ID);

			rs = pstmt.executeQuery();
			int no = 0;
	
			while (rs.next()) {
				int m_M_Product_ID=rs.getInt(1);
				BigDecimal PriceStd=rs.getBigDecimal(9);
				if (wu.getC_BPartner_ID() >0)
				{	
					MProductPricing pp = new MProductPricing (m_M_Product_ID, wu.getC_BPartner_ID(),Env.ONE, true);
					MPriceListVersion pv=new MPriceListVersion(ctx, m_Pricelist_Version_ID,null);
					
					pp.setM_PriceList_ID(pv.getM_PriceList_ID());
					pp.setM_PriceList_Version_ID(m_Pricelist_Version_ID);
				//	pp.setPriceDate(date);
					PriceStd=pp.getPriceStd();
				}
				// if not all records limit list
				if (!allRecords && ++no > MAX_LINES) {
					m_notAllPrices = true;
					break;
				}
			}
		} catch (Exception e) {
			log.log(Level.SEVERE, "load", e);
		} finally {
			DB.close(rs, pstmt);
			rs = null;
			pstmt = null;
		}
		
	} // load


	/**
	 * Not all Prices displayed
	 * 
	 * @return true if no prices
	 */
	public boolean isNotAllPrices() {
		return m_notAllPrices;
	} // isNotAllPrices

	/**
	 * Get Prices
	 * 
	 * @return Price Array List
	 */
	

	/**
	 * Get Search Info
	 *
	 * @return search info
	 */
	public String getSearchInfo() {
		return m_searchInfo;
	} // getSearchInfo

	/*************************************************************************/

	/**
	 * Get Name
	 * 
	 * @return Price List Name
	 */
	public String getName() {
		return m_name;
	}

	public String getDescription() {
		return m_description;
	}

	public String getCurrency() {
		return m_currency;
	}

	public String getCurSymbol() {
		return m_curSymbol;
	}

	public String getAD_Language() {
		return m_AD_Language;
	}

	public boolean isTaxIncluded() {
		return m_taxIncluded;
	}



} // PriceList
