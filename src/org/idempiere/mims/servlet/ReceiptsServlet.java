/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2006 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package org.idempiere.mims.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.compiere.model.I_M_InOutLineMA;
import org.compiere.model.MDocType;
import org.compiere.model.MDocumentStatus;
import org.compiere.model.MInOut;
import org.compiere.model.MInOutConfirm;
import org.compiere.model.MInOutLine;
import org.compiere.model.MInOutLineMA;
import org.compiere.model.MIssue;
import org.compiere.model.MMailMsg;
import org.compiere.model.MOrder;
import org.compiere.model.MOrderLine;
import org.compiere.model.MPayment;
import org.compiere.model.MProduct;
import org.compiere.model.MStorageOnHand;
import org.compiere.model.MTable;
import org.compiere.model.Query;
import org.compiere.model.X_EP_Production;
import org.compiere.process.DocAction;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.compiere.util.WebEnv;
import org.compiere.util.WebSessionCtx;

import org.compiere.util.WebUtil;
import org.idempiere.mims.WebUserIMS;


/**
 *  Web Order.
 *
 *  @author Jorg Janke
 *  @version $Id: OrderServlet.java,v 1.2 2006/07/30 00:53:21 jjanke Exp $
 */
@WebServlet(
		name="receiptsServlet",
        urlPatterns = "resource/receiptsServlet"
)
public class ReceiptsServlet extends HttpServlet
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -94012227184686536L;

	/**	Logging						*/
	private static CLogger			log = CLogger.getCLogger(ReceiptsServlet.class);
//	private static Logger	s_log = Logger.getCLogger(OrderServlet.class);

	/** Name						*/
	static public final String			NAME = "receiptsServlet";

	/**
	 *	Initialize global variables
	 *
	 *  @param config Configuration
	 *  @throws ServletException
	 */
	public void init(ServletConfig config)
		throws ServletException
	{
		super.init(config);
		if (!WebEnv.initWeb(config))
			throw new ServletException("ReceiptsServlet.init");
	}   //  init

	/**
	 * Get Servlet information
	 * @return Info
	 */
	public String getServletInfo()
	{
		return "Adempiere Web Receipts Servlet";
	}	//	getServletInfo

	/**
	 * Clean up resources
	 */
	public void destroy()
	{
		log.fine("");
	}   //  destroy

	
	/**************************************************************************
	 *  Process the HTTP Get request.
	 * 	(logout, deleteCookie)
	 *  Sends Web Request Page
	 *
	 *  @param request request
	 *  @param response response
	 *  @throws ServletException
	 *  @throws IOException
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException
	{
		if (log.isLoggable(Level.INFO)) log.info("Get from " + request.getRemoteHost() + " - " + request.getRemoteAddr());


		StringBuilder answer = new StringBuilder();
		Properties ctx = JSPEnv.getCtx(request);

		String productEAN = WebUtil.getParameter (request, "productean");
		if (productEAN!=null) productEAN=productEAN.replaceAll("[^0-9?!\\.]","");
		String productToDeleteEAN = WebUtil.getParameter (request, "producteantodel");
		int m_inoutline_id= WebUtil.getParameterAsInt(request, "m_inoutline_id");
		int m_inout_id= WebUtil.getParameterAsInt(request, "m_inout_id");
		boolean confirmreceipt= WebUtil.getParameterAsBoolean(request, "confirmreceipt");
		
		if (confirmreceipt)
			answer.append( createconfirmation(m_inout_id, ctx));
		else if (m_inoutline_id==0 && m_inout_id==0)
			answer.append("03"); // System error
		else
		{
			
			if (productEAN == null && productToDeleteEAN !=null )	{
				answer.append( deleteLine(m_inoutline_id,productToDeleteEAN,ctx)); // Create LOT for shipment line
			} 
			else
			{
				answer.append( addLine( m_inoutline_id, m_inout_id, productEAN,  ctx));
				
			}
		}	
		//
		response.setHeader("Cache-Control", "no-cache");
		response.setContentType("text/html; charset=UTF-8");
		PrintWriter out = response.getWriter();     //  with character encoding support
		out.write(URLEncoder.encode(answer.toString(), "UTF-8"));
		out.flush();
		if (out.checkError())
			log.log(Level.SEVERE, "error writing");
		out.close();
	}	//	doGet

	
	/**
	 *  Process the HTTP Post request
	 *
	 *  @param request request
	 *  @param response response
	 *  @throws ServletException
	 *  @throws IOException
	 */
	public void doPost (HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException
	{
		if (log.isLoggable(Level.INFO)) log.info("Post from " + request.getRemoteHost() + " - " + request.getRemoteAddr());
		Properties ctx = JSPEnv.getCtx(request);
		HttpSession session = request.getSession(true);
		session.removeAttribute(WebSessionCtx.HDR_MESSAGE);

	
		String orderSearch = WebUtil.getParameter (request, "pallets");
		if (orderSearch != null)
			ctx.put("ReceiptSearch", orderSearch);
		
		String dateto = WebUtil.getParameter (request, "dateend");
		if (dateto != null)
			ctx.put("dateto", dateto);
		String datefrom = WebUtil.getParameter (request, "datestart");
		if (datefrom != null)
			ctx.put("datefrom", datefrom);
	
		//	Web User/Basket
		WebUserIMS wu = (WebUserIMS)session.getAttribute(WebUserIMS.NAME);
		
		String url = "/resource/receiptDetails.jsp?M_InOut_ID=1018799"; // paymentInfo.jsp
		//	Not logged in
		if (wu == null || !wu.isLoggedIn())
		{
			session.setAttribute("CheckOut", "Y");	//	indicate checkout
			url = "/login.jsp";
		}

		if (log.isLoggable(Level.INFO)) log.info ("Forward to " + url);
		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher (url);
		dispatcher.forward (request, response);
	}	//	doPost

	public String deleteLine(int m_inoutline_id,String productToDeleteEAN, Properties ctx){
		 // DELETE PALLETE FROM receipt
		MInOutLine inoutline= new MInOutLine( ctx,m_inoutline_id,null);
		
		if (productToDeleteEAN.isEmpty())
			return "01"; //Pallete doesnot exists
		else
		{
			X_EP_Production pallete=new Query(ctx, X_EP_Production.Table_Name, "documentno=?", null)
			.setParameters(productToDeleteEAN)
			.first();
	
			
			
			if (pallete==null)
				return "01" ;
			else 
			{
				if (inoutline.getM_Product_ID()!=pallete.getM_Product_ID())
					return "02";
				else
				{
					DB.executeUpdate("delete from "+MInOutLineMA.Table_Name+" where M_InOutLine_ID="+inoutline.get_ID()+" AND M_AttributeSetInstance_ID="+pallete.getM_AttributeSetInstance_ID(),  null);
					BigDecimal qtyprocessed= DB.getSQLValueBDEx(null, "select coalesce(sum(movementqty),0) from m_inoutlinema where M_InOutLine_ID=?", inoutline.getM_InOutLine_ID());
					inoutline.setPickedQty(qtyprocessed);
					inoutline.saveEx();
					return "OK";
				}
			}
		}
	
	}
	
	private String addLine(int m_inoutline_id,int m_inout_id ,String productEAN, Properties ctx) {
		
		Env.setCtx(ctx);
		

	
		if (productEAN.isEmpty())
		return"01"; //Product doesnot exists
		else
		{
			MProduct product=new Query(ctx,MProduct.Table_Name, "upc=?", null)
			.setParameters(productEAN)
			.setClient_ID()
			.setOnlyActiveRecords(true)
			.first();
	
			if (product==null)
				return "01";
			else 
			{
				if (m_inoutline_id!=0 )
				{
					
					MInOutLine inoutline= new MInOutLine( ctx,m_inoutline_id,null);
					if (inoutline.getM_Product_ID()!=product.getM_Product_ID())
					{
						List<MProduct> substitutes = new Query(ctx, MProduct.Table_Name, " m_product_id in (SELECT s.substitute_id from M_Substitute s where s.M_Product_ID=?) ", null)
								.setParameters(product.getM_Product_ID())
								.setClient_ID()
								.setOnlyActiveRecords(true)
								.list();
						
						
						for (MProduct substitute:substitutes){
							if (substitute.getM_Product_ID()==inoutline.getM_Product_ID()){
								
								
								
								BigDecimal qtyprocessed= inoutline.getMovementQty();
								if (qtyprocessed.add(Env.ONE).compareTo(inoutline.getMovementQty())>0 )
									return "05";
								inoutline.setPickedQty(qtyprocessed.add(Env.ONE));
								inoutline.saveEx();
								return "OK";
							}
						}
						return "02";
					}
					else
					{
						BigDecimal qtyprocessed=  inoutline.getMovementQty();
						if (qtyprocessed.add(Env.ONE).compareTo(inoutline.getMovementQty())>0 )
								return "05";
						inoutline.setPickedQty(qtyprocessed.add(Env.ONE));
						inoutline.saveEx();
						return "OK";
					}
				}
				else 
				{
					MInOut  inout= new MInOut( ctx,m_inout_id,null);
					MInOutLine[] inoutlines = inout.getLines();
					for (int i=0;i<inoutlines.length;i++){
							
						MInOutLine inoutline =  inoutlines[i];
						if (inoutline.getM_Product_ID()!=product.getM_Product_ID())
							continue;
						else
						{
							
//							if (qtyprocessed.add(Env.ONE).compareTo(inoutline.getMovementQty())>0 )
//								return "05";
//							else {
								inoutline.setPickedQty(inoutline.getMovementQty().add(Env.ONE));
								inoutline.setMovementQty(inoutline.getPickedQty());
								inoutline.setQtyEntered(inoutline.getPickedQty());
								inoutline.saveEx();
								return "OK";
//							}
							
						}
						
					}
					
					MInOutLine line=new MInOutLine(inout);
					line.setM_Product_ID(product.getM_Product_ID());
					line.setQty(Env.ONE);
					line.setM_Locator_ID(product.getM_Locator_ID());
					line.saveEx();
					return "OK";
				}
					
/*					for (int i=0;i<inoutlines.length;i++){
						MInOutLine inoutline =  inoutlines[i];
						List<MProduct> substitutes = new Query(ctx, MProduct.Table_Name, " m_product_id in (SELECT s.substitute_id from M_Substitute s where s.M_Product_ID=?) ", null)
								.setParameters(pallete.getM_Product_ID())
								.setClient_ID()
								.setOnlyActiveRecords(true)
								.list();
						for (MProduct substitute:substitutes){
							if (substitute.getM_Product_ID()==inoutline.getM_Product_ID()){
								MInOutLineMA minoutlinema = new Query(ctx, I_M_InOutLineMA.Table_Name, "M_InOutLine_ID=? AND M_AttributeSetInstance_ID=? ", 
										inoutline.get_TrxName())
									.setParameters(inoutline.getM_InOutLine_ID(), pallete.getM_AttributeSetInstance_ID())
									.first();
								if (minoutlinema!=null)
									return "04";
								
								BigDecimal qtyprocessed= DB.getSQLValueBDEx(null, "select coalesce(sum(movementqty),0) from m_inoutlinema where M_InOutLine_ID=?", inoutline.getM_InOutLine_ID());
								if (qtyprocessed.add(pallete.getQty()).compareTo(inoutline.getMovementQty())>0 )
									return "05";
								
								inoutline.setPickedQty(qtyprocessed.add(pallete.getQty()));
								inoutline.saveEx();
								return "OK";
							}
						}
					}*/
				
			}
		}
		
	
	}

	
	public String createconfirmation(int m_inout_id, Properties ctx)
	{
		Env.setCtx(ctx);
		MInOut inout = new MInOut (ctx,m_inout_id,null);
		MInOutConfirm confirmation= MInOutConfirm.create (inout, MInOutConfirm.CONFIRMTYPE_PickQAConfirm, true);
		if (confirmation.isProcessing())
			return "OKC";
		
		if ((confirmation.getDocStatus().equals(DocAction.STATUS_Drafted)) 
				&& (confirmation.processIt(DocAction.ACTION_Complete))) {
			confirmation.saveEx();
			return "OKC";
		}
		else 
			return "07";
	}
	

	
}	//	OrderServlet
