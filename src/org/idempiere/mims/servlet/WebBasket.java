/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2006 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package org.idempiere.mims.servlet;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

import javax.servlet.http.HttpSession;

import org.compiere.model.MAccount;
import org.compiere.model.MQuery;
import org.compiere.model.Query;
import org.compiere.model.X_W_Basket;
import org.compiere.model.X_W_BasketLine;
import org.compiere.util.Env;
import org.compiere.util.WebSessionCtx;
import org.idempiere.mims.WebUserIMS;



/**
 *  Web Basket
 *
 *  @author Jorg Janke
 *  @version $Id: WebBasket.java,v 1.2 2006/07/30 00:53:21 jjanke Exp $
 */
public class WebBasket
{
	/**
	 * 	Constructor
	 */
	public WebBasket(WebUserIMS wu, 	WebSessionCtx wsc , Properties ctx  )
	{
		try {
			
		if (wu==null || wsc==null || !wu.isLoggedIn())
			return;
		
		basket=new Query(ctx, X_W_Basket.Table_Name, X_W_Basket.COLUMNNAME_C_BPartner_ID+"=? AND "+X_W_Basket.COLUMNNAME_AD_User_ID+"=? AND W_Store_ID=? ", null )
			.setParameters(wu.getC_BPartner_ID(),wu.getAD_User_ID(),wsc.wstore.getW_Store_ID())
		 	.first();
		
		if ( basket==null) 
		{	
				basket=new X_W_Basket(ctx, 0, null);
				basket.set_ValueNoCheck("AD_Client_ID", wu.getAD_Client_ID());
				basket.setAD_User_ID(wu.getAD_User_ID());
				basket.setC_BPartner_ID(wu.getC_BPartner_ID());
				basket.setM_PriceList_ID(m_PriceList_ID);
				basket.setEMail(wu.getEmail());
				basket.setW_Store_ID(wsc.wstore.getW_Store_ID());
				basket.saveEx();
		} else
		{
			basketlines=new Query(ctx, X_W_BasketLine.Table_Name, " W_Basket_ID=? ", null )
					.setParameters(basket.getW_Basket_ID())
					.list();	
			for(X_W_BasketLine bl : basketlines)
			{
				 add (bl.getM_Product_ID(),bl.getM_Product().getName(), bl.getQty(), bl.getPrice(),bl.getDescription(),bl.getW_BasketLine_ID(),ctx, false);
			}
		}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}	//	WebBasket

	
	private X_W_Basket basket=null;
	/**	Attribute Name - also in JSPs	*/
	public static final String		NAME = "webBasket";
	
	/**	Lines						*/
	private ArrayList<WebBasketLine>	m_lines = new ArrayList<WebBasketLine>();
	private  List<X_W_BasketLine>	basketlines = new ArrayList<X_W_BasketLine>();
	/** Total w/o tax				*/
	private BigDecimal		m_total;
	private BigDecimal		m_itemqty=Env.ZERO;
	/**	Line (max) counter			*/
	private int				m_lineNo = 0;
	private int 			m_PriceList_Version_ID = -1;
	private int 			m_PriceList_ID = -1;
	/**	Sales Rep					*/
	private int 			m_SalesRep_ID = 0;
	private String 			m_Description = ""; 
	
	/**
	 * 	String Representation
	 * 	@return info
	 */
	public String toString()
	{
		StringBuilder sb = new StringBuilder("WebBasket[Lines=");
		sb.append(m_lines.size()).append(",Total=").append(m_total)
			.append(",M_PriceList_ID=" + m_PriceList_ID)
			.append("]");
		return sb.toString();
	}	//	toString

	/**
	 * 	Get Total
	 *	@return	total
	 */
	public BigDecimal getTotal ()
	{
		return getTotal(false);
	}	//	getTotal

	/**
	 * 	Get (recalculated) Total
	 *	@return	total
	 */
	public BigDecimal getTotal (boolean recalc)
	{
		if (recalc)
		{
			m_total = Env.ZERO;
			for (int i = 0; i < m_lines.size(); i++)
			{
				WebBasketLine wbl = (WebBasketLine)m_lines.get(i);
				m_total = m_total.add(wbl.getTotal());
			}
		}
		if (m_total == null)
			return Env.ZERO;
		return m_total;
	}	//	getTotal

	public int getItemCount ()
	{
		return (getItemCount(false));
	}
	
	public int getItemCount (boolean recalc)
	{
		if (recalc)
		{
			m_itemqty = Env.ZERO;
			for (int i = 0; i < m_lines.size(); i++)
			{
				WebBasketLine wbl = (WebBasketLine)m_lines.get(i);
				m_itemqty = m_itemqty.add(wbl.getQuantity());
			}
		}
		if (m_itemqty == null)
			return 0;
		return m_itemqty.intValue();
	}	//	getTotal
	/**
	 * 	Get Line Count
	 *	@return line count
	 */
	public int getLineCount()
	{
		return m_lines.size();
	}	//	getLineCount

	/**
	 *	Get Lines
	 * 	@return lines
	 */
	public ArrayList<WebBasketLine> getLines()
	{
		return m_lines;
	}	//	getLines

	/**
	 * 	Add Line
	 *	@param wbl line
	 *	@return added line
	 */
	public WebBasketLine add (WebBasketLine wbl)
	{
		wbl.setLine (m_lineNo++);
		m_lines.add (wbl);
		getTotal (true);
		getItemCount(true);
		return wbl;
	}	//	add
	
	/**
	 * 	Add Line.
	 * 	Adds qty to the line, if same product 
	 * 	@param M_Product_ID product
	 * 	@param Name Name
	 *	@param Qty Qty
	 * 	@param Price Price
	 *	@return created / modified line
	 */	
	public WebBasketLine add (int M_Product_ID, String Name, BigDecimal Qty, BigDecimal Price, String sDescription,Properties ctx ,boolean uniqueline )
	{
		return add ( M_Product_ID,  Name,  Qty,  Price,  sDescription, 0,ctx , uniqueline);
	}	//	add


	public WebBasketLine add (int M_Product_ID, String Name, BigDecimal Qty, BigDecimal Price, String sDescription, int X_W_BasketLine_ID,Properties ctx ,boolean uniqueline)
	{
		if (basket==null)
			return null;

		if (!uniqueline){
		//	try adding to existing line
			for (int i = 0; i < m_lines.size(); i++)
			{
				WebBasketLine wbl = (WebBasketLine)m_lines.get(i);
				if (wbl.getM_Product_ID() == M_Product_ID)
				{
					
					wbl.addQuantity (Qty);
					X_W_BasketLine bl=new X_W_BasketLine(ctx,wbl.getbasketline_id() ,basket.get_TrxName());
					bl.setQty(wbl.getQuantity());
					bl.saveEx();
					getTotal (true);
					getItemCount(true);
					return wbl;
				}
			}
		}
		//	new line
		
		
		if (X_W_BasketLine_ID==0)
		{
			X_W_BasketLine bl=new X_W_BasketLine(ctx,X_W_BasketLine_ID,basket.get_TrxName());
			bl.set_ValueNoCheck("AD_Client_ID", basket.getAD_Client_ID());
			bl.setW_Basket_ID(basket.get_ID());
			bl.setM_Product_ID(M_Product_ID);
			bl.setProduct(NAME);
			bl.setPrice(Price);
			bl.setDescription((sDescription==null?" ":sDescription));
			bl.setQty(Qty);
			bl.setLine((1+m_lines.size())*10);
			//System.out.println("store_id="+basket.getW_Store_ID()+ " basket_id="+basket.getW_Basket_ID()+" bl_id="+bl.get_ID()+" ad_client_id="+bl.getAD_Client_ID());
			bl.saveEx();
			X_W_BasketLine_ID=bl.get_ID();
			basketlines.add(bl);
		}
		WebBasketLine wbl = new WebBasketLine (M_Product_ID, Name, Qty,	Price, sDescription);
		wbl.setbasketline_id(X_W_BasketLine_ID);
		return add (wbl);
	}
	
	/**
	 * 	Delete Line
	 *	@param no line no
	 */
	public void delete (int no,Properties ctx)
	{
		try {
			for (int i = 0; i < m_lines.size(); i++)
			{
				WebBasketLine wbl = (WebBasketLine)m_lines.get(i);
				if (wbl.getLine() == no)
				{
					
					X_W_BasketLine bl= new X_W_BasketLine(ctx,basketlines.get(i).get_ID(),null);
					
					if (basketlines.size()==0){
						basket.deleteEx(false);
						basket=null;
					}
					else 
					{
						bl.deleteEx(true);
					}
					
					basketlines.remove(i);
					m_lines.remove(i);
					getTotal(true);
					getItemCount(true);
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}	//	delete

	public void emptyBasket ()
	{
		m_lines.clear();
		basketlines.clear();
		basket.deleteEx(false);
		basket=null;
		getTotal(true);
		getItemCount(true);
		
	}	//	delete

	public int getM_PriceList_Version_ID()
	{
		return m_PriceList_Version_ID;
	}
	public void setM_PriceList_Version_ID(int PriceList_Version_ID)
	{
		if (PriceList_Version_ID > 0)
			m_PriceList_Version_ID = PriceList_Version_ID;
	}
	
	
	public int getM_PriceList_ID()
	{
		return m_PriceList_ID;
	}
	public void setM_PriceList_ID(int PriceList_ID)
	{
		if (PriceList_ID > 0)
			m_PriceList_ID = PriceList_ID;
	}

	
	/**
	 * @return Returns the salesRep_ID.
	 */
	public int getSalesRep_ID ()
	{
		return m_SalesRep_ID;
	}
	/**
	 * @param salesRep_ID The salesRep_ID to set.
	 */
	public void setSalesRep_ID (int salesRep_ID)
	{
		m_SalesRep_ID = salesRep_ID;
	}
	
	public X_W_Basket getBasket()
	{
		return basket;
	}
}	//	WebBasket
