/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2006 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package org.idempiere.mims.servlet;

import java.util.Properties;
import java.util.logging.Level;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.jstl.core.Config;
import javax.servlet.jsp.tagext.TagSupport;

import org.compiere.util.CLogger;
import org.compiere.util.Env;


/**
 * 	Product Tag.
 * 	Loads Price List
 *  <pre>
 *  <cws:priceList priceList_ID="0"/>
 *  Variable used = "priceList"
 *	</pre>
 *
 *  @author Jorg Janke
 *  @version $Id: PriceListTag.java,v 1.2 2006/07/30 00:53:21 jjanke Exp $
 */
public class ProductDetailTag extends TagSupport
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -2109422988040998327L;

	/**	Price List ID			*/
	private int					m_product_ID = 0;
	
	private int					m_Pricelist_Version_ID = 0;
	
	private ProductDetail		m_productdetail;

	/**	Logger							*/
	private CLogger				log = CLogger.getCLogger (getClass());

	/**
	 * 	Set Price List
	 * 	@param var price list
	 */
	public void setm_Pricelist_Version_ID (String var)
	{
		try
		{
			m_Pricelist_Version_ID = Integer.parseInt (var);
		}
		catch (NumberFormatException ex)
		{
			log.warning("m_Pricelist_Version_ID - " + ex.toString());
		}
	}	//	setM_PriceList_ID


	/**
	 * 	Set Price List
	 * 	@param var price list
	 */
	public void setm_product_ID (String var)
	{
		try
		{
			m_product_ID = Integer.parseInt (var);
		}
		catch (NumberFormatException ex)
		{
			log.warning("setPriceList_ID - " + ex.toString());
		}
	}	//	setM_PriceList_ID


	
	/**
	 *  Start Tag
	 *  @return SKIP_BODY
	 * 	@throws JspException
	 */
	public int doStartTag() throws JspException
	{
		//	Create Price List
		Properties ctx = JSPEnv.getCtx((HttpServletRequest)pageContext.getRequest());
		int AD_Client_ID = Env.getContextAsInt(ctx, "AD_Client_ID");
			//	Get Parameters

		m_productdetail = ProductDetail.get (ctx, AD_Client_ID, m_product_ID, m_Pricelist_Version_ID,
			null, null, false, (HttpServletRequest)pageContext.getRequest());
		
		
		//	Set Price List
		HttpSession session = pageContext.getSession();
		session.setAttribute (ProductDetail.NAME, m_productdetail);
		if (log.isLoggable(Level.FINE)) log.fine("PL=" + m_productdetail);

		//	Set Locale from Price List
		//
		return (SKIP_BODY);
	}   //  doStartTag

	/**
	 * 	End Tag
	 * 	@return EVAL_PAGE
	 * 	@throws JspException
	 */
	public int doEndTag() throws JspException
	{
		return EVAL_PAGE;
	}	//	doEndTag

}	//	PriceListTag
